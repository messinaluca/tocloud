
# Guida all'utilizzo

## Conversione del notebook e crezione dell'immagine:

* Automatica: avviare docker ed eseguire il notebook, verrà creata l'immagine **tocloud**

* Manuale: avviare docker ed eseguire i comandi:

```{r, eval=F, echo=T, include=T}

 jupyter nbconvert --to script main.ipynb

 docker build -t [nomedelcontainer] .
 
```

## Sorgenti dati

* I csv da utilizzare sono reperibili su:

  + https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/vaccinations.csv
  
  + https://raw.githubusercontent.com/datasets/population/master/data/population.csv
  
Nel caso si volessero passare i propri sorgenti dati, copiarli all'interno della directory montata, rinominandoli rispettivamente "vaccini.csv" e "popolazione.csv". Nel caso non venissero forniti il programma scaricherà ed utilizzerà le ultime versioni disponibili.


## Run del container 

La sintassi di seguito è esemplificativa e comprensiva di:

* passaggio variabili d'ambiente attraverso file;

* bind mount della cartella di esecuzione verso la cartella "materiale" del container;

* utilizzo della metrica "vaccine";

* specifica di un formato di output;

* invio su canale Telegram (solo con parametri settati);

## Lista delle opzioni disponibili

```{r, eval=F, echo=T, include=T}

docker run --env-file ./env.list -v "${PWD}/:/home/materiale" [nomedelcontainer] --h

```

**Requistiti**:

Nessun parametro

**Opzionale**:

Nessun parametro

**Risultati restituiti**:

* la lista dei comandi per interagire con il container.

## Lista dei paesi disponibili

```{r, eval=F, echo=T, include=T}

docker run --env-file ./env.list -v "${PWD}/:/home/materiale" [nomedelcontainer]
--country_list --format [print, txt, csv] --send

```

**Requistiti**:

* un formato di output;

**Opzionale**:

L'opzione --send inoltra i risultati sul canale Telegram.

**Risultati restituiti**:

* la lista dei paesi disponibili.

* Sulla base del format scelto:

  + format [print]: printa su schermo la lista dei paesi disponibili;
  
  + format [txt]: salva il file "country_list.txt" contenente la lista dei paesi disponibili;
  
  + format [csv]: salva il file "country_list.csv" contenente la lista dei paesi disponibili:



## La Metrica "vaccine"

```{r, eval=F, echo=T, include=T}

docker run --env-file ./env.list -v "${PWD}/:/home/materiale" [nomedelcontainer]
--vaccine country threshold --format [print, txt, csv] --send

```

**Requistiti**:

* Un paese che sia presente in lista;

* un valore di soglia >= 60;

* un formato di output;

In caso i parametri non soddisfino questi requisiti o non venga fornito il numero corretto di parametri, il programma non restituisce nulla e logga un messaggio d'errore. 

**Opzionale**:

L'opzione --send inoltra i risultati sul canale Telegram.


**Risultati restituiti**:

* il grafico "previsione_vaccinati_country.png" di una previsione a tre mesi della percentuale di individui vaccinati, prodotta dal modello Prophet;

* Sulla base del format scelto:

  + format [print]: printa su schermo una desrizione estesa del risultato;
  
  + format [txt]: salva il file "vaccini_country.txt" contenente la desrizione estesa del risultato;
  
  + format [csv]: salva i file "vaccini_country.csv" e "prophet_country.csv" rappresentanti:
  
    + Una stima delle dosi mancanti per raggiungere l'immunità di gregge;
    
    + Una stima della data in cui l'obbiettivo vaccinale verrebbe raggiunto;
    
    + Una stima delle dosi medie giornaliere che andrebbero somministrate per raggiungere l'obbiettivo entro il 30 luglio e il 30 agosto 2020;
    
    + Una previsione a tre mesi della percentuale di individui vaccinati, prodotta dal modello Prophet;
    
**Esempio di esecuzione della metrica**

```{r, eval=F, echo=T, include=T}

docker run --env-file ./env.list -v "${PWD}/:/home/materiale" [nomedelcontainer]
--vaccine Italy 80 --format [print, txt, csv] --send

```
    
