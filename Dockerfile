FROM ubuntu:18.04
RUN apt update
RUN apt install -y python3-pip
RUN pip3 install --upgrade pip
RUN mkdir -p /home/materiale
COPY req.txt main.py /home
ADD sticker /home/sticker
WORKDIR /home
RUN pip3 install -r req.txt
RUN rm req.txt
ENV PROD "True"
ENTRYPOINT ["python3","main.py"]
