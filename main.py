#!/usr/bin/env python
# coding: utf-8

# In[1]:


#NASCONDERE EVENTUALI WARNING DI PANDAS PER COPIA DEL DF
#pd.options.mode.chained_assignment = None  # default='warn'
import pandas as pd
import requests
from collections import defaultdict
import datetime as dt
import numpy as np
import matplotlib.pyplot as plt
import random
import pathlib 
from pathlib import Path
import sys
import time
import csv
import os
import logging
import datetime
from datetime import datetime, date, timedelta
from prophet import Prophet
import plotly


os.environ['USER'] = os.getenv("USER", "Anonymous")
user = os.getenv("USER", "Anonymous")
################################
#PATH DOVE SALVARE IL MATERIALE#
################################

#CONSIDERIAMO PATH ASSOLUTO (EVENTUALE SVILUPPO CON CRON)
path_abs = str(pathlib.Path().parent.absolute())
#PATH DI DESTINAZIONE DEL MATERIALE GENERATO
path_materiale = "{}/materiale/results/".format(path_abs)
#path_materiale = str(path_abs+"/materiale/")
file_path = "{}/materiale/".format(path_abs)
#file_path = str(path_abs+"/data/")


##################
#GRAFICI##########
##################
val_dpi = 300


def check_dir():
    if os.path.isdir(path_materiale) == False:
        try: 
            os.mkdir(path_materiale)

        except OSError as error:
            return(["error",'Errore nella creazione della directory "{}"'.format(path_materiale)])

    if os.path.isdir(path_materiale) == True:
        return(["info",'La directory "{}" è stata creata correttamente'.format(path_materiale)])

res = check_dir()


logging.basicConfig(handlers=[logging.FileHandler(filename=path_materiale+'main.log', 
                                                 encoding='utf-8', mode='a+')],
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    level=logging.INFO)
#Possiamo loggare tutti gli errori da debug in su (info, warning, error e critical)
#logging.basicConfig(filename=path_materiale+'main.log', filemode='a+', format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger()

if res[0] == "info":
    logger.info(res[1])
if res[0] == "error":
    logger.error(res[1])


# In[2]:


#####################################
#CONTROLLO ESISTENZA PERCORSI E FILE
#COLLEZIONO LOG
#####################################
#se si trova nella cartella principale lo carico da la e il path è abs sennò lo carico dentro materiale in modo da darlo anche all'utente
file_locale = ""
def down_file(url, filename):
    if "vaccini" in filename:
        filename = "input_vaccini.csv"
    if "popolazione" in filename:
        filename = "input_popolazione.csv"
    logger.info("Non è stato trovato la sorgente {}. Lo sto scaricando dalla rete".format(filename))

    try:
        response = requests.get(url)
        if response.status_code == 200:
            with open(file_path+filename, 'wb') as f:
                for chunk in response.iter_content():
                    if chunk: # filter out keep-alive new chunks
                        f.write(chunk)
            #log.append({"desc":"Scaricata sorgente dati dati da {}".format(url),"code":response})
            logger.info("{} è stato scaricato, testo della response: {}".format(filename,response) )
        else:
            logger.warning("Il sito ha risposto un codice errore diverso da 200 ({})".format(response.status_code))

    except Exception as e:
        logger.error("Qualcosa è andato storto nello scaricare {}\n{}".format(filename,e))
    
    return file_path+filename


#ritorna il filename 
def check_file():
    file_list = []
    
    # Get the list of all files and directories
    dir_list = os.listdir(file_path)
    #controlliamo se esistono entrambi i file, sennò scarichiamo
    f_v = f_p = False
    for file in dir_list:
        if "csv" and "popolazione" in file:
            f_p = True
            file_list.append({'popolazione': "{}/{}".format(file_path,file)}) 
            
        if "csv" and "vaccini"  in file:
            f_v = True
            file_list.append({'vaccini': "{}/{}".format(file_path,file)}) 
        
        
    #se sono presenti entrambi esco e elaboro dalla dir principale       
    if f_v and f_p == True:
        logger.info("I csv vaccini e popolazione sono stati trovati in locale {}".format(file_path))
        return file_list
    
    #li scarico in materiale e li fornisco    
    else:
        logger.info("Non sono stati trovati entrambi i csv popolazione e vaccini in locale {}".format(file_path))
        file_list = []
        url = "https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/vaccinations.csv"
        logger.info("Scarico il csv vaccini da\n{}".format(url))
        file_list.append({'vaccini': down_file(url,"vaccini")})

        url = "https://raw.githubusercontent.com/datasets/population/master/data/population.csv"
        logger.info("Scarico il csv popolazione da\n{}".format(url))
        file_list.append({'popolazione': down_file(url,"popolazione")})
        
        return file_list


# In[3]:


'''
SETTAGGI PER BOT TELEGRAM
FUNZIONI PER INVIO VERSO CANALE TELEGRAM.
PRENDE COME ARGOMENTI L'ID DEL CANALE SU CUI INVIARE I MESSAGGI E L'OGGETTO NOTE
'''

import telepot

#ID DEL CANALE SU CUI TRASMETTERE
id_canale = os.getenv("TG_CH", None)

#TOKEN DEL BOT (DA CREARE VIA TELEGRAM APP)
token = os.getenv("TG_TK", None)

enable_telegram = ""
if os.getenv("TG_CH") is None or os.getenv("TG_TK") is None :
    logger.info('Telegram, non sono presenti valori per canale e token')
    enable_telegram = False
if os.getenv("TG_CH") is not None and os.getenv("TG_TK") is not None: 
    logger.info('I parametri scelti per Telegram sono:canale {} token {}'.format(os.getenv("TG_CH"),os.getenv("TG_TK")))
    enable_telegram = True
    

if enable_telegram == True:
    #ISTANZIO IL BOT
    bot = telepot.Bot(token)


    def send_to_channel(id_canale,obj_note):
        logger.info('Inizio caricamento su canale telegram')

        header = obj_note.get_header()
        if len(header) == 0:
            logger.info('Telegram, nessun messaggio da inviare')
            return

        with open(path_abs+"/sticker/plane.webp", 'rb') as file_toup:
            try:
                response = bot.sendSticker(id_canale, file_toup)
                logger.info('Telegram, caricamento sticker {}'.format(response))
            except Exception as e:
                logger.error("Qualcosa è andato storto nel caricamento stikers.\nSe non hai usato i parametri di default di telegram, controlla che siano corretti\n{}".format(e))



        for item, content in header.items():
            msg = content["details"]["msg"]
            mat_url = path_materiale+content["details"]["mat_url"]

            #DISTINZIONE TRA TIPO DI DOCUMENTO PER L'INVIO
            if content["type"] == "img":
                with open(mat_url, 'rb') as file_toup:
                    response = bot.sendPhoto(id_canale, file_toup,caption = "Invio da parte di {}\n{}".format(user,msg))
                    logger.info('Telegram, caricamento immagine {}'.format(response))
                    time.sleep(2)

            if content["type"] == "doc":
                with open(mat_url, 'rb') as file_toup:
                    response = bot.sendDocument(id_canale, file_toup)
                    logger.info('Telegram, caricamento documento {}'.format(response))
                    time.sleep(2)

            if content["type"] == "text":
                bot.sendMessage(id_canale,msg,parse_mode='HTML' )
                logger.info('Telegram, invio messaggio terminato {}'.format(response))
                time.sleep(2)

        response = bot.sendMessage(id_canale,"<i>Aggiornamento completato</i>",parse_mode='HTML' )
        logger.info('Telegram, aggiornamento completato {}'.format(response))

    class note(object):
        logger.info('Telegram, creata istanza')

        def __init__(self):
            self.header = defaultdict(dict)


        def add_msg(self,item,tipo,testo,url_materiale):    
            payload = {"msg":testo,"mat_url":url_materiale}
            self.header[item].update({"type":tipo,"details":payload})
            logger.info('Telegram, messaggio aggiunto alla lista')


        '''
        STAMPA GLI ELEMENTI COLLEZIONATI IN HEADER
        '''
        def read(self):
            for i,j in self.header.items():
                print("{} {}".format(i,j))

        '''
        RESTITUISCE L'OGGETTO HEADER
        '''
        def get_header(self):
            return self.header


    #INSTANZIAMO OGGETTO NOTE
    rel_tg = note()


# In[4]:


#######
# INIT#
#######
check_dir()
filename = check_file()

file_vaccini = file_popolazione = ""
#LETTURA DEL FILE E CREAZIONE DEL DATAFRAME
for i in filename:
    if "vaccini" in i:
        file_vaccini = str(i["vaccini"])
    if "popolazione" in i:
        file_popolazione= str(i["popolazione"])

try:
    df_vaccini = pd.read_csv(file_vaccini, encoding = "utf-8 ")
    logger.info("Il csv vaccini è convertito in dataframe correttamente")
    
    df_popolazione = pd.read_csv(file_popolazione, encoding = "utf-8 ")
    logger.info("Il csv popolazione è convertito in dataframe correttamente")
    
except Exception as e:
    logger.error("Qualcosa è andato storto nella conversione del csv a dataframe \n{}".format(e))


# In[5]:


def vaccine(df_v,df_p, country, threshold,tipo_formato):
    #print("citta {}".format(type(country)))
    if tipo_formato == "":
        tipo_formato = "print"
        
    input_vaccini = df_v
    input_population = df_p
 
    tabella = pd.DataFrame()
        
    i = threshold/100
        
    vacc_italy = input_vaccini[input_vaccini["location"] == country] 
    dati_vaccini = vacc_italy["total_vaccinations"]
    pop_country = input_population[input_population["Country Name"] == country]
    popolazione = int( pop_country["Value"].tail(1) )
    
    # elaborazioni
    perc_vacc = 100*dati_vaccini/popolazione 
    daily_mean = dati_vaccini.diff().tail(7).mean() 
    dosi_mancanti = (i*2*popolazione) - dati_vaccini 
    giorni_mancanti = dosi_mancanti.tail(1)/daily_mean 
    data = date.today() + timedelta(days=int(giorni_mancanti)) 
    den1 = datetime.strptime('2021-07-30', '%Y-%m-%d').date() - date.today() 
    media1 = dosi_mancanti.tail(1)/den1.days 
    den2 = datetime.strptime('2021-08-30', '%Y-%m-%d').date() - date.today()
    media2 = dosi_mancanti.tail(1)/den2.days 
    
    # previsioni prophet
    dati_perc = 100*((dati_vaccini/2)/popolazione)
    train_df = pd.concat([vacc_italy["date"], dati_perc], axis = 1)
    train_df.columns = ["ds", "y"]
    
    m = Prophet(daily_seasonality=True)
    m.fit(train_df)
    future = m.make_future_dataframe(periods=90)
    forecast = m.predict(future)
    fig = m.plot(forecast, xlabel='', ylabel="")
    ax = fig.gca()
    ax.set_title("Percentage of vaccinated people in {}".format(country))
    
    fig.savefig("{}previsione_vaccinati_{}.png".format(path_materiale,country),bbox_inches='tight',dpi = val_dpi)
    if enable_telegram == True:
        rel_tg.add_msg("previsione_vaccinati_{}.png".format(country),"img","previsione_vaccinati_{}".format(country),"previsione_vaccinati_{}.png".format(country))

    # save output
    values = {"Missing doses (K)":[int(dosi_mancanti.tail(1)/1000)],
     "Date of herd immunity":[data],
     "Avg. daily doses by 2021-07-30":[int(media1)],
     "Avg. daily doses by 2021-08-30":[int(media2)],
     "Threshold":[threshold]}
    
    tabella = pd.DataFrame(values)
    tabella_prophet = forecast[["ds","yhat"]]

    text = '''
    Currently {} is missing {} million doses to vaccinate {}% of the population with vaccine and booster.
    At the current vaccination rate, equal to {} thousand daily doses, herd immunity would be reached by {}.
    To reach the goal by 2021-07-30, an average of {} thousand doses should be administered per day.
    To reach the goal by 2021-08-30, an average of {} thousand doses should be administered per day.
    '''.format(country,str(int( dosi_mancanti.tail(1)/1000000)),str(threshold),str(int(daily_mean/1000)),str(data),str( int(media1/1000) ),str( int(media2/1000) ) )
    
    if tipo_formato == "txt":
        with open("{}vaccini_{}_{}.txt".format(path_materiale,country,threshold), "w") as file:
            file.write(text)
        if enable_telegram == True:
            rel_tg.add_msg("vaccini_{}_{}.txt".format(country,threshold),"doc","invio txt vaccini","vaccini_{}_{}.txt".format(country,threshold))


    if tipo_formato == "csv":
        tabella.to_csv("{}vaccini_{}_{}.csv".format(path_materiale,country,threshold),index=False)
        if enable_telegram == True:
            rel_tg.add_msg("vaccini_{}_{}.csv".format(country,threshold),"doc","invio csv vaccini","vaccini_{}_{}.csv".format(country,threshold))
        
        tabella_prophet.to_csv("{}prophet_{}_{}.csv".format(path_materiale,country,threshold),index=False)
        if enable_telegram == True:
            rel_tg.add_msg("prophet_{}_{}.csv".format(country,threshold),"doc","invio csv prophet","prophet_{}_{}.csv".format(country,threshold))
        
    if tipo_formato == "print":
        print("{}".format(text))
        if enable_telegram == True:
            rel_tg.add_msg("vaccini_print","text",text,"vaccini_print")

    
def country_list(df_v,df_p,tipo_formato):
    lista_paesi = np.intersect1d(df_v["location"].unique(), df_p["Country Name"].unique())
    lista_paesi = ', '.join(np.sort(lista_paesi))
    
    if tipo_formato == "txt":
        with open("{}country_list.txt".format(path_materiale), "w") as file:
            file.write(lista_paesi)
        if enable_telegram == True:
            rel_tg.add_msg("country_list.txt","doc","country list","country_list.txt")

    if tipo_formato == "print":
        print(lista_paesi)
        if enable_telegram == True:
            rel_tg.add_msg("country_list","text",lista_paesi,"country_list")
    
    if tipo_formato == "csv":
        with open("{}country_list.csv".format(path_materiale), "w") as file:
            file.write(lista_paesi)
        if enable_telegram == True:
            rel_tg.add_msg("country_list.csv","doc","country list","country_list.csv")

    return lista_paesi
        
lista_paesi = country_list(df_vaccini,df_popolazione,"")


# In[6]:


import argparse
# create parser
parser = argparse.ArgumentParser()

parser.add_argument('--format',
                    dest='format',
                    help='Choose the format for saving the output files',
                    choices=('txt', 'csv','print'),
                    default = 'print',
                    nargs='+',
                    required = True
                    )

parser.add_argument('--vaccine',
                    dest='vaccine',
                    help='Choose the metric to compute (vaccine) and its arguments (country, threshold>=60)',
                    nargs='+'
                    )

parser.add_argument('--country_list',
                    dest='country_list',
                    help='Get the list of input countries',
                    action='store_true'
                   )


parser.add_argument('--send',
                    dest='send',
                    help='Send all files created from metric to telegram channel. Valid ID and Token are needed',
                    action='store_true'
                    )
 
args = parser.parse_args()
# get the arguments value
formato = ""
if args.format:
    formato = args.format[0]

if args.country_list:    
    country_list(df_vaccini,df_popolazione,formato)
    logger.info('Chiamata funzione lista citta')

        
if args.vaccine:
    try:
        if args.vaccine[0] and args.vaccine[1]:     
            #se i parametri non rispettano le direttive vado di default
            if args.vaccine[0] not in lista_paesi.split(", ") or int(args.vaccine[1])<60 :
                #print("Threshold must be greater than 60 or country not in list, switched to default value Italy 60")
                args.vaccine[0] = "Italy"
                args.vaccine[1] = 60
                logger.info('Paese o soglia non rispettano i valori richiesti.')
                msg = 'Paese o soglia non rispettano i valori richiesti.\nLa soglia deve esser maggiore di 60\nIl paese deve essere nella lista (parametro --country_list)'
                print('{}'.format(msg))
                os._exit(0)
        vaccine(df_vaccini,df_popolazione, args.vaccine[0], int(args.vaccine[1]), formato)
        logger.info('Chiamata funzione vaccini con parametri paese = {} e soglia = {} e format = {}'.format(args.vaccine[0],int(args.vaccine[1]),formato))
    

    except:
        #se non sono passati entrambi i parametri vado di default
        #logger.info('Paese o soglia non sono stati inseriti. Chiamata funzione vaccini con parametri standard paese = {} soglia = {} perchè mancanti'.format("Italy",60))
        logger.info('Paese o soglia non sono stati inseriti.')
        msg = "Paese o soglia non sono stati inseriti\nRicorda che soglia deve essere almeno 60 e che il paese deve essere in lista (parametro --country_list)\nEsempio chiamata --vaccine Italy 60" 
        print('{}'.format(msg))
        os._exit(0)
        #vaccine(df_vaccini,df_popolazione, country = "Italy", threshold = 60, tipo_formato = formato)



if args.send:
    if enable_telegram == True:
        send_to_channel(id_canale,rel_tg)
    else:
        print("Non son stati inseriti parametri per telegram") 


# In[ ]:


logger.info("--------------FINE------------")


# In[ ]:


#SE VAR PROD NON è SETTATA CONVERTE IL NOTEBOOK, CANCELLA EVENTUALE BUILD PRECEDENTE E NE CREA UNA NUOVA
if os.getenv("PROD") is None :
    
    command = "jupyter nbconvert --to script main.ipynb"
    os.system(command)
 
    #stoppo ed eliminoeventuali contenitori aperti in modo da poter cancellare e ribuildare l'immagine senza crearne di nuove
    import subprocess
    container_ids = subprocess.check_output(['docker', 'ps', '-aq'], encoding='ascii')
    container_ids = container_ids.strip().split()
    if container_ids:
        subprocess.check_call(['docker', 'stop'] + container_ids)
        subprocess.check_call(['docker', 'rm'] + container_ids)
        
    command = "docker rmi tocloud"
    os.system(command)
    
    command = "docker build -t tocloud ."
    os.system(command)


# In[ ]:




